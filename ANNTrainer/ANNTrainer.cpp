#include <iostream>
#include <ANN.h>
using namespace std;
using namespace ANN;

void main()
{
	cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;
	vector<size_t> init;
	init.resize(4);
	init = { 2, 3, 3, 1 };
	auto pAnn = CreateNeuralNetwork(init);
	vector<vector<float>> inputs, outputs;
	if (!LoadData("xor.data", inputs, outputs))
	{
		cout << "Error! File is not open, inputs and outputs are empty." << endl;
		return ;
	}
	BackPropTraining(pAnn, inputs, outputs, 50000, 1e-1, 0.1, true);
	pAnn->Save("xor_my.ann");
	pAnn->Save("C:\\Users\\544\\Documents\\������\\vsevolod\\ANNSample\\xor_my.ann");
}