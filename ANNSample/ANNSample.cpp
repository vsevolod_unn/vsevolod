#include <iostream>
#include <ANN.h>
#include <string>
using namespace std;
using namespace ANN;

void main()
{
	cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;
	vector<vector<float>> inputs, outputs;
	if (!LoadData("xor.data", inputs, outputs))
	{
		cout << "Error! File is not open, inputs and outputs are empty." << endl;
		return;
	}
	vector<size_t> init;
	init.resize(4);
	init = { 2, 3, 3, 1 };
	auto pAnn = CreateNeuralNetwork(init);

	if (!pAnn->Load("xor_my.ann"))
	{
		cout << "Error! Neural network isn`t load" << endl;
		return;
	}
	cout << pAnn->GetType() << endl;
	cout << "inputs\t" << "\tetalon\t" << "network predict" << endl;
	for (int i(0); i < 4; i++)
	{
		cout << inputs[i][0] << "\t" << inputs[i][1] << "\t" << outputs[i][0] << "\t" << pAnn->Predict(inputs[i])[0] << endl;;
	}
}